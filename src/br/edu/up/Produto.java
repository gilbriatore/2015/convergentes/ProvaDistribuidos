package br.edu.up;

public class Produto {

  private Integer id;
  private String nome;
  private String valor;
  
  public Produto(Integer id, String nome, String valor) {
    super();
    this.id = id;
    this.nome = nome;
    this.valor = valor;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getValor() {
    return valor;
  }

  public void setValor(String valor) {
    this.valor = valor;
  }
}