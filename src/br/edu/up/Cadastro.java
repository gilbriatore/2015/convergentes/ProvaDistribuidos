package br.edu.up;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Cadastro extends LinkedHashMap<Integer, Produto> {

  private static final long serialVersionUID = 3816497500290876649L;
  private static Cadastro singleton;

  //singleton
  private Cadastro() {
    incluir(new Produto(1, "Pneu", "R$ 100,00"));
    incluir(new Produto(2, "Bateria", "R$ 150,00"));
    incluir(new Produto(3, "SmartTV", "R$ 1.500,00"));
  }

  public static Cadastro getCadastro() {
    if (singleton == null) {
      singleton = new Cadastro();
    }
    return singleton;
  }

  public List<Produto> listarProdutos() {
    return new ArrayList<Produto>(this.values());
  }

  public Produto buscarPorId(Integer id) {
    return get(id);
  }

  public void incluir(Produto produto) {
    put(produto.getId(), produto);
  }

  public void excluir(Integer id) {
    remove(id);
  }
}